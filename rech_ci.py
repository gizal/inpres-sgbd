import setup_ci as v
import http.server
import json
import re
import urllib.parse
import bisect

PORT = 44444

print("[%s] >>> chargements des indexes" % (__file__,))

# on charge toutes les vues
for view_name in v.views:
    v.fetch_view(view_name)

# effectuer la recherche d'un mot dans un index
def search_word_in_index(word, index_name):
    index = v.get_index(index_name)
    results_id = []

    # si il y a une * à la fin on ne compare que le début en parcourant le tableau par bissection
    if word[-1]=="*":
        word = word[:-1]
        # on récupère les clés triées
        keys = v.get_index_keys(index_name)
        # on fait une recherche de l'index d'insertion avant notre valeur
        pos = bisect.bisect_left(keys, word)
        # on lit le tableau et ajoute au résultat tant que ça correspond à la recherche
        while pos < len(keys) and keys[pos].startswith(word):
            results_id.extend(index[keys[pos]])
            pos += 1

    # sinon on recherche juste par mot ( directement avec la hash table de python )
    elif word in index:
        results_id = index[word]

    return results_id

# effectuer la recherche d'une fourchette dans l'index
def search_fourchette_in_index(low, high, index_name):
    index = v.get_index(index_name)
    results_id = []

    # on récupère les clés triées
    keys = v.get_index_keys(index_name)
    # on fait une recherche de l'index d'insertion avant notre valeur low
    pos = bisect.bisect_left(keys, low)
    # on lit le tableau et ajoute au résultat tant que c'est plus bas que high
    while pos < len(keys) and keys[pos] <= high:
        results_id.extend(index[str(keys[pos])])
        pos += 1

    return results_id

# effectuer une recherche, paramètre = int ou map
def search(data):
    results_id = []

    # si c'est un entier on a notre résultat
    if type(data) is int:
        results_id.append(str(data))
    # sinon c'est une recherche par critère
    else:
        first = True

        # on regarde chacun des champs "texte"
        for view_name in ("title", "actors_name", "directors_name", "genres_name", "certification"):
            # si l'élément est dedans
            if view_name in data:
                # on coupe la phrase de recherche en mot
                for part in data[view_name].lower().split():
                    # on cherche la mot dans l'index
                    films = search_word_in_index(part, view_name)
                    # pour le cas du titre, on fait la recherche dans les titres originaux et on combine les résultats
                    if "title" == view_name:
                        films = set(films) | set(search_word_in_index(part, "original_title"))
                    # si aucun résultat, on retourne aucun résultat
                    if not films:
                        return []
                    # sinon si la list était vide on la remplit
                    elif first :
                        results_id = films
                        first = False
                    # sinon on fait l'intersection
                    else:
                        results_id = set(results_id).intersection(films)
                        # si le résultat est vide, on retourne aucun résultat
                        if not results_id:
                            return []

        # on regarde chacun des champs "nombres"
        for view_name in ("release_date", "vote_average", "vote_count"):
            # si une borne pour la vue n'est pas dans la requête on continue
            if view_name+"_low" not in data and view_name+"_high" not in data:
                continue

            # si le low est dedans, on l'ajoute sinpn on prend 0
            low = float(data[view_name+"_low"]) if view_name+"_low" in data else 0

            # si le high est dedans, on l'ajoute sinon on prend l'infini
            high = float(data[view_name+"_high"]) if view_name+"_high" in data else float("Infinity")
            
            # on swap si l'ordre n'est pas bon ( si l'utilisateur l'a entré à l'envers dans l'application  )
            if low > high:
                low, high= high, low

            films = search_fourchette_in_index(low, high, view_name)
            
            # si aucun résultat, on retourne aucun résultat
            if not films:
                return []
            # sinon si la list était vide on la remplit
            elif first :
                results_id = films
                first = False
            # sinon on fait l'intersection
            else:
                results_id = set(results_id).intersection(films)
                # si le résultat est vide, on retourne aucun résultat
                if not results_id:
                    return []

    results = []
    for result_id in results_id:
        results+= get_result(result_id)

    return results;
        

# get int and transform in date
def datify(date):
    if date is None:
        return ""
    return re.sub('([0-9]+)-([0-9]{2})-([0-9]{2})', r'\3/\2/\1', date)

# récupérer un résultat
def get_result(id):
    if id in v.get_view("title"):
        result = [ id ]
        result += [ v.get_view("title")[id][0] ]
        result += [ v.get_view("original_title").get(id, [""])[0] ]
        result += [ ", ".join(v.get_view("actors_name").get(id, [""])) ]
        result += [ ", ".join(v.get_view("directors_name").get(id, [""])) ]
        result += [ datify(v.get_view("release_date").get(id, [""])[0]) ]
        result += [ ", ".join(v.get_view("genres_name").get(id, [""])) ]
        result += [ v.get_view("vote_average").get(id, [""])[0] ]
        result += [ v.get_view("vote_count").get(id, [""])[0] ]
        result += [ v.get_view("certification").get(id, [""])[0] ]
        return [ result ]
    else:
        return []

class ServerRech(http.server.BaseHTTPRequestHandler):
    # on redéfinit la méthode do_GET ( qui est appelée quand le serveur reçoit une requête GET )
    def do_GET(self):
        # on met le code de réponse à 200
        self.send_response(200)
        # si l'adresse de la page demandée commence par /search?q= il s'agit d'une recherche
        if self.path.startswith("/search?q="):
            # on prend la partie après q= et la décode par json
            data = json.loads(urllib.parse.unquote(self.path[10:]))
            self.send_header("Content-type", "application/javascript")
            self.end_headers()
            # on fait la recherche et retourne le contenu display_result([resultat1, resultat2, resultat3, ...])
            self.wfile.write(bytes("display_result("+json.dumps(search(data))+")", "utf-8"))
            return  
        # si c'est une page de demande d'importation d'une sélection d'id
        elif self.path.startswith("/import?ids="):
            # on prend la partie après ids=
            data = self.path[12:].split(",")
            self.send_header("Content-type", "application/javascript")
            self.end_headers()
            # on affiche une alerte comme quoi on a bien reçu ce qu'il faut
            self.wfile.write(bytes("alert('Le serveur a bien reçu la demande d\\'importation de "+", ".join(data)+"')", "utf-8"))
            return

        # sinon on est dans la page d'affichage du formulaire
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(bytes(open("index.html").read(), "utf-8"))
        print("/* "+self.path+" */")

print("[%s] >>> serveur démarré à l'adresse http://127.0.0.1:%d" % (__file__, PORT))

# on lance un serveur web ( adresse == "" c'est un cas particulier pour dire localhost, et le port est mis au dessus )
# on lui passe comme handler la classe ServerRech qui reçevra les requêtes
serveur_web = http.server.HTTPServer(('', PORT), ServerRech)
# on boucle sur le serveur web qui va servir les requêtes jusqu'à ce qu'on le coupe
serveur_web.serve_forever()
