import urllib.request
import os
import json
import re

views_data= {}
indexes_data= {}
indexes_keys= {}

server_url = "http://127.0.0.1:5984/"
main_dd = "movies/_design/main"

def fetch_view(view_name):
    """Fetch a view"""

    # if already in memory, do nothing
    if view_name in views_data:
        return

    # if in file, load it
    if os.path.exists("cache/"+view_name):
        views_data[view_name]= json.loads(open("cache/"+view_name).read())
        print("[%s] &&& vue %s chargée : %s" % (__file__, view_name, "cache/"+view_name))
        indexes_data[view_name]= json.loads(open("cache/"+view_name+".index").read())
        print("[%s] &&& index %s chargé : %s" % (__file__, view_name, "cache/"+view_name+".index"))
        indexes_keys[view_name]= json.loads(open("cache/"+view_name+".keys").read())
        print("[%s] &&& clés de l'index %s chargé : %s" % (__file__, view_name, "cache/"+view_name+".keys"))
        return

    # load view data
    print("[%s] chargement %s" % (__file__, server_url + main_dd + "/_view/"+view_name))
    view_raw_data = json.loads(urllib.request.urlopen(server_url + main_dd + "/_view/"+view_name).read().decode('utf-8'))

    # transform {"id":"movie_id", "key": "value_of_interest", "value": "null_or_movie_id"} in {"movie_id": "value_of_interest"}
    view_data= {}
    for l in view_raw_data["rows"]:
        if l["key"] is None:
            continue
        if l["id"] in view_data:
            view_data[l["id"]]+= [l["key"]]
        else:
            view_data[l["id"]]= [l["key"]]

    # save view to file
    with open("cache/"+view_name, 'w') as f:
        json.dump(view_data, f, separators=(',',':'))

    print("[%s] +++ vue %s chargée et cachée : %s" % (__file__, view_name, "cache/"+view_name))

    index_data = {}

    # if view is of type enumber
    if view_name in number_views:

        for key in view_data:

            for val in view_data[key]:

                # ignore bad value
                if not isinstance(val, float) and not isinstance(val, int):
                    continue

                # append movie id to an array list associated to key in index
                if val in index_data:
                    index_data[round(val, 1)].append(key)
                else:
                    index_data[round(val, 1)]= [key]

    # elif it's a date
    elif view_name in date_views:
        for key in view_data:

            for val in view_data[key]:

                if not isinstance(val, str):
                    continue
                match = re.match('^([0-9]{4})-([0-1][0-9])-([0-3][0-9])$', val)
                if match is not None:
                    # put it in integer form
                    val = (int(match.group(1))*100 + int(match.group(2))) * 100 + int(match.group(3))
                    if val in index_data:
                        index_data[val].append(key)
                    else:
                        index_data[val]= [key]


    # else it's string
    else:
        for key in view_data:

            for val in view_data[key]:
                # ignore bad value
                if not isinstance(val, str):
                    continue

                # divide string in words
                for word in set(re.findall('\w+', val.lower(), re.UNICODE)):

                    # append movie id to an array list associated to word in index
                    if word in index_data:
                        index_data[word].append(key)
                    else:
                        index_data[word]= [key]

    # write index
    with open("cache/"+view_name+".index", 'w') as f:
        json.dump(index_data, f, separators=(',',':'))

    print("[%s] +++ index %s chargé et caché : %s" % (__file__, view_name, "cache/"+view_name+".index"))

    indexes_data[view_name]= index_data

    # sorted keys
    indexes_keys[view_name]= sorted(index_data.keys())

    # write index keys
    with open("cache/"+view_name+".keys", 'w') as f:
        json.dump(indexes_keys[view_name], f, separators=(',',':'))

    print("[%s] +++ clés de l'index %s chargé et caché : %s" % (__file__, view_name, "cache/"+view_name+".keys"))

def get_view(view_name):
    """Get a view ( dictionary of 'movie id -> data' )"""
    fetch_view(view_name)
    return views_data[view_name]

def get_index(view_name):
    """Get an index ( dictionary of 'data -> [ movie id, movie id, ... ]' )"""
    fetch_view(view_name)
    return indexes_data[view_name]

def get_index_keys(view_name):
    """Get an sorted keys of an index"""
    fetch_view(view_name)
    return indexes_keys[view_name]

# vues
views = ('actors_name', 'title', 'original_title', 'directors_name', 'release_date', 'genres_name', 'certification', 'vote_average', 'vote_count')

# vues avec de multiples fois le même id de film
multi_views = ('actors_name', 'directors_name', 'genres_name')

# vues numeriques
number_views = ('vote_average', 'vote_count')

# vue date
date_views = ('release_date',)
