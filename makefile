.PHONY:	play clean

play:
	python rech_ci.py

rmori:
	rm -f cache/original_title{,.index}

clean:
	rm -f cache/*
